var gulp = require('gulp'),
	include = require('gulp-include'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify');


gulp.task('process-js', function(){
	console.log('processing js');

	gulp.src('assets/javascripts/application.js')
		.pipe(include())
			.on('error', console.log)
		.pipe(uglify())
		.pipe(gulp.dest('dist/javascripts'));
});

gulp.task('process-sass', function(){
	console.log('processing sass/css');

	gulp.src('assets/stylesheets/application.scss')
		.pipe(sass.sync({outputStyle: 'compressed'}))
		.pipe(gulp.dest('dist/stylesheets'));
});

gulp.task('copy-fonts', function() {
	console.log('copying fonts');

	gulp.src([
		'assets/bower/font-awesome/fonts/*',
		'assets/bower/bootstrap/fonts/*',
		'assets/fonts/*'])
		.pipe(gulp.dest('dist/fonts'));
});

gulp.task('copy-images', function(){
	console.log('copying images');

	gulp.src('assets/images/**')
	.pipe(gulp.dest('dist/images'));
});

gulp.task('copy-src', function(){
	console.log('copying templates');

	gulp.src('src/**')
	.pipe(gulp.dest('dist'));
});

gulp.task('watch', function(){
	gulp.watch([
		'assets/fonts/**',
		'assets/images/**',
		'assets/javascripts/**',
		'assets/stylesheets/**',
		'src/**'		
		], ['default']);
});

gulp.task('default', [
	'process-js', 
	'process-sass', 
	'copy-fonts',
	'copy-images', 
	'copy-src']);