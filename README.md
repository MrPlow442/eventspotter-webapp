# Event Spotter web aplikacija#

Ovdje se nalaze upute za pokretanje Event Spotter web aplikacije

### Što je sve potrebno ###

* [Node.js](https://nodejs.org/en/)
* [Bower](http://bower.io/)
* [Gulp](http://gulpjs.com/)
* [Apache HTTP 2.4](http://httpd.apache.org/download.cgi)

### Kako pokrenuti ###

* Instalirajte Node.js
* Instalirajte Bower
* Instalirajte Gulp
* Instalirajte Apache server
* Kopirajte EventSpotter-webapp direktorij s CDa na željeno mjesto na disku
* U EventSpotter-webapp direktoriju pokrenite: 
```
#!

bower install
```

* U EventSpotter-webapp direktoriju pokrenite: 
```
#!

npm install --save
```

* U EventSpotter-webapp direktoriju pokrenite: 
```
#!

gulp
```
* Kopirajte novo nastali sadržaj dist direktorija u apache/htdocs direktorij
* Pokrenite apache server koristeći
```
#!

./httpd
```
naredbu u apache24/bin direktoriju

# Event Spotter web application #

This is a guide on how to run the event spotter web application

### Requirements ###

* [Node.js](https://nodejs.org/en/)
* [Bower](http://bower.io/)
* [Gulp](http://gulpjs.com/)
* [Apache HTTP 2.4](http://httpd.apache.org/download.cgi)

### How to start ###

* Install Node.js
* Install Bower
* Install Gulp
* Install Apache server
* Copy EventSpotter-webapp directory from CD to desired location
* In EventSpotter-webapp directory run: 
```
#!

bower install
```

* In EventSpotter-webapp directory run: 
```
#!

npm install --save
```

* In EventSpotter-webapp directory run: 
```
#!

gulp
```
* Copy newly generated contents of dist directory to apache/htdocs directory
* Start apache server by running
```
#!

./httpd
```
in apache24/bin directory