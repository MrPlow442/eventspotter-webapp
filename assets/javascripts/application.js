// POLYFILLS
//= require polyfills/array-find.js
//= require polyfills/array-filter.js

// DEPENDENCIES
//= require ../bower/jquery/dist/jquery.js
//= require ../bower/lodash/lodash.js
//= require ../bower/angular/angular.js
//= require ../bower/angular-google-maps/dist/angular-google-maps.js
//= require ../bower/angular-messages/angular-messages.js
//= require ../bower/angular-animate/angular-animate.js
//= require ../bower/a0-angular-storage/dist/angular-storage.js
//= require ../bower/angular-translate/angular-translate.js
//= require ../bower/angular-translate-loader-static-files/angular-translate-loader-static-files.js
//= require ../bower/ui-router/release/angular-ui-router.js
//= require ../bower/ng-file-upload/ng-file-upload-all.js
//= require ../bower/angular-bootstrap/ui-bootstrap-tpls.js
//= require ../bower/slick-carousel/slick/slick.js
//= require ../bower/angular-slick/dist/slick.js
//= require ../bower/angular-bootstrap-show-errors/src/showErrors.js

// CUSTOM COMPONENTS
//= require app/components/scroll/scroll-directive.js
//= require app/components/scroll/scroll-service.js
//= require app/components/scroll/math-util-service.js
//= require app/components/geolocation/geolocation.js
//= require app/components/preload/preload.js
//= require app/components/preload/preload-directive.js
//= require app/components/preload/preload-service.js


// COMPONENTS
//= require app/components/app-components.js
//= require app/components/app-constants.js
//= require app/components/auth/auth.js
//= require app/components/modal/modal.js
//= require app/components/service/service.js
//= require app/components/filter/filter.js
//= require app/components/directive/directive.js
//= require app/components/**/*.js

// MODULES
//= require app/public/about/about.js
//= require app/public/about/about-controller.js

//= require app/public/events/events.js
//= require app/public/events/modal/event-details-service.js
//= require app/public/events/modal/event-details-controller.js
//= require app/public/events/events-controller.js

//= require app/public/organize-event/organize-event.js
//= require app/public/organize-event/organize-event-controller.js

//= require app/public/public.js

//= require app/app.js
//= require app/app-controller.js
//= require app/init.js