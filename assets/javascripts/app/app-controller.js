console.log("app-controller.js");
(function () {
    'use strict';

    angular
        .module('app')
        .controller('AppController', AppController);

    AppController.$inject = [
        '$scope', '$state', '$translate',
        'USER_ROLE', 'DATE_FORMAT',
        'AuthService', 'TokenService', 'AuthVerifierService', 'AuthModalService', 'AlertService', 'GeolocationService',
        'ProfileModalService', 'MyFavoritesModalService', 'MyEventsModalService',
        'languages'];

    function AppController(
        $scope, $state, $translate,
        USER_ROLE, DATE_FORMAT,
        AuthService, TokenService, AuthVerifierService, AuthModalService, AlertService, GeolocationService,
        ProfileModalService, MyFavoritesModalService, MyEventsModalService,
        languages) {
        var vm = this;

        window.appControllerDebug = vm;

        vm.alerts = AlertService.getAll();
        vm.languages = languages;
        vm.USER_ROLE = USER_ROLE;
        vm.DATE_FORMAT = DATE_FORMAT;

        vm.datepickerOptions = {
            class: 'custom-datepicker'
        };

        // public methods
        vm.language = language;
        vm.logout = logout;
        vm.isAuthenticated = isAuthenticated;
        vm.getUsername = AuthService.getUsername;
        vm.hasRoles = hasRoles;
        vm.openAuthModal = openAuthModal;
        vm.openMyFavoritesModal = openMyFavoritesModal;
        vm.openMyEventsModal = openMyEventsModal;
        vm.isGeolocationSupported = GeolocationService.isGeolocationSupported;
        vm.getLocationAndRedirectToEvents = getLocationAndRedirectToEvents;

        init();

        function init() {
            AuthVerifierService.startVerifier();
        }

        // private methods
        function logout() {
            var token = TokenService.get();
            if(token) {
                AuthService.logout(token);
            }
            //$state.go('home');
        }

        function isAuthenticated() {
            return AuthService.isAuthenticated();
        }

        function hasRoles(roles) {
            return AuthService.hasRoles(roles);
        }

        function openAuthModal(event, data) {
            if(AuthService.isAuthenticated()) {
                ProfileModalService.open()
                    .result
                    .then(angular.noop); //leaving this here in case some additional logic will be required
            } else {
                AuthModalService.open()
                    .result
                    .then(function(response){
                        if(response.success && angular.isDefined(data)) {
                            $state.go(data.stateName, data.stateParams);
                        }
                    });
            }
        }

        function openMyFavoritesModal(event, data) {
            MyFavoritesModalService.open()
                .result
                .then(angular.noop); //leaving this here in case some additional logic will be required
        }

        function openMyEventsModal(event, data) {
            MyEventsModalService.open()
                .result
                .then(angular.noop); //leaving this here in case some additional logic will be required
        }

        function getLocationAndRedirectToEvents(options) {
            GeolocationService
                .getCurrentPosition(options)
                .then(redirect)
                .catch(errors);

            //////
            function redirect(response) {
                $state.go("events", {lat: response.coords.latitude, lng: response.coords.longitude});
            }

            function errors(error) {
                console.error(error);
            }
        }

        // listeners
        $scope.$on('openAuthModal', openAuthModal);

        function language(locale) {
            $scope.$broadcast("language-changed", $translate.use(locale));
        }
    }
})();