console.log("app.js");
(function () {
    'use strict';

    angular
        .module('app', [
            //custom plugins
            'simpleScroll',
            'geolocation',
            'preload',
            // plugins
            'ngAnimate',
            'ngMessages',
            'ui.router',
            'ui.bootstrap',
            'ngFileUpload',
            'slick',
            'pascalprecht.translate',
            'ui.bootstrap.showErrors',

            // components sub-module
            'app.components',

            // public sub-module
            'app.public'
        ]);

    angular
        .module('app')
        .config(config)
        .run(run);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$translateProvider', 'datepickerPopupConfig'];
    run.$inject = ['$rootScope', '$templateCache', '$state', 'AuthService'];

    function config($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider, datepickerPopupConfig) {

        console.log("Application config initializing");

        // From: https://docs.angularjs.org/api/ng/provider/$httpProvider
        // "configure $http service to combine processing of multiple http responses received at around the same time...
        // this can result in significant performance improvement for bigger applications that make many HTTP requests concurrently...""
        $httpProvider.useApplyAsync(true);

        // STATES
        $stateProvider
        // Initial application state
        .state('app', {
            url: '',
            abstract: true,
            controller: 'AppController as app',
            templateUrl: 'templates/layout.html',
            resolve: {
                languages: ['CodebookService', function(CodebookService){
                    return CodebookService
                        .languages()
                        .then(function(result){
                            return result.data;
                        });
                }]
            }
        })
        // Public initial state
        .state('public', {
             url: '/',
             parent: 'app',
             views: {
                'about': {
                    templateUrl: 'templates/public/about.html',
                    controller: 'AboutController as about'
                },
                'organize-event': {
                    templateUrl: 'templates/public/organize-event.html',
                    controller: 'OrganizeEventController as organizeEvent',
                    resolve: {
                        categories: ['CodebookService', function(CodebookService){
                            return CodebookService
                                .categories({showAll: true})
                                .then(function(response){
                                    return response.data;
                                });
                        }],
                        daysOfWeek: ['CodebookService', function(CodebookService){
                            return CodebookService
                                .daysOfWeek()
                                .then(function(response){
                                   return response.data;
                                });
                        }]
                    }
                }
             }
        });

        // If an unknown uri is hit, redirect to root '/'
        $urlRouterProvider.otherwise('/');

        // INTERCEPTORS
        // HTTP interceptor which adds authorization header to XHR requests tagged with auth: true
        $httpProvider.interceptors.push(['$injector', function($injector){
            return {
                request: function(config) {
                    var AuthService = $injector.get('AuthService');
                    var TokenService = $injector.get('TokenService');

                    if(AuthService.isAuthenticated() && !!config.auth) {
                        var token = TokenService.get();

                        var header = {
                            'Authorization': token.token_type + " " + token.access_token
                        };

                        config.headers = angular.extend(config.headers, header);
                    }

                    return config;
                }
            };
        }]);


        datepickerPopupConfig.showButtonBar = false;

        // INTERNATIONALIZATION
        $translateProvider.useStaticFilesLoader({
            prefix: 'lang/locale-',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('en_US');
    }

    function run($rootScope, $templateCache, $state, AuthService) {
        console.log('App started...');

        $rootScope.$on('$stateChangeStart', authCheck);
        //
        ////TODO:
        //$rootScope.$on('$stateChangeError', stateError);

        // validation messages
        $templateCache.put("error-messages",
            '<p ng-message="required" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_REQUIRED\' | translate }}</p>' +
            '<p ng-message="pattern" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_PATTERN\' | translate }}</p>' +
            '<p ng-message="maxlength" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_MAX_LENGTH\' | translate }}</p>' +
            '<p ng-message="minlength" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_MIN_LENGTH\' | translate }}</p>' +
            '<p ng-message="date" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_DATE\' | translate }}</p>' +
            '<p ng-message="time" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_TIME\' | translate }}</p>' +
            '<p ng-message="email" class="help-block text-bold">{{ \'VALIDATION_MESSAGE_EMAIL\' | translate }}</p>' +
            '<p ng-message="confirmPassword" class="help-block text-bold">{{ \'VALIDATION_CONFIRM_PASSWORD\' | translate }}</p>'
        );

        function authCheck(event, toState, toParams) {

            var allowedRoles = toState.data ? toState.data.allowedRoles : undefined;

            if(!allowedRoles) {
                return;
            }

            AuthService.validate()
                .then(verifyRoles, forceLogin);

            function verifyRoles(response) {
                console.log(response);
                if(!AuthService.hasRoles(allowedRoles)) {
                    console.log("User doesn't have permission, redirecting");
                    event.preventDefault();
                    //$state.go('401');
                }
            }

            function forceLogin(response) {
                console.log(response);
                console.log("Not logged in, forcing login");
                event.preventDefault();
                //$state.go('home');
                $rootScope.$broadcast('openAuthModal', {stateName: toState.name, stateParams: toParams});
            }

        }
    }

})();