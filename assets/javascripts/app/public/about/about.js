(function () {
    'use strict';

    angular
        .module('app.public.about', []);

    angular
        .module('app.public.about')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
    }
})();