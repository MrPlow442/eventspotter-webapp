(function () {
    'use strict';

    angular
        .module('app.public', [
            'app.public.about',
            'app.public.events',
            'app.public.organizeEvent'
        ]);
})();
