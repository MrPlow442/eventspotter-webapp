(function () {
    'use strict';

    angular
        .module('app.public.events')
        .controller('EventsController', EventsController);

    EventsController.$inject = [
        '$scope',
        'WEATHER_CONDITION_CODE', 'GMAPS_OPTIONS', 'ICONS',
        'SimpleScrollService', 'PreloadService', 'EventDetailsModalService', 'MathService',
        'categories','latitude', 'longitude', 'events', 'weather'];

    function EventsController(
        $scope,
        WEATHER_CONDITION_CODE, GMAPS_OPTIONS, ICONS,
        SimpleScrollService, PreloadService, EventDetailsModalService, MathService,
        categories, latitude, longitude, events, weather) {
        var vm = this;

        window.evts = this;

        vm.user = {
            id: "usr",
            location: {
                latitude: latitude,
                longitude: longitude
            },
            icon: ICONS.PERSON_FOCUS
        };

        var allEvents = events.content;
        vm.events = events.content;
        vm.mapMarkers = vm.events.concat(vm.user);
        vm.weather = weather;
        vm.categories = categories;
        vm.selectedCategory = undefined;

        vm.pagination = {
            size: 5,
            events: 1
        };

        vm.paginationOptions = {
            maxSize: 10,
            boundaryLinks: true,
            rotate: false
        };

        vm.map = {
            center: {
                latitude: latitude,
                longitude: longitude
            },
            zoom: 12,
            events: {
                tilesloaded: function(map) {
                    $scope.$apply(onTilesLoaded);
                }
            }
        };

        vm.mapOptions = GMAPS_OPTIONS;
        vm.selectedMarker = {};
        vm.showWindow = false;


        vm.filterEventsByCategory = filterEventsByCategory;
        vm.showOnMap = showOnMap;
        vm.openDetailsModal = openDetailsModal;
        vm.convertDtToDay = convertDtToDay;
        vm.convertToCelsiusAndRound = convertToCelsiusAndRound;
        vm.getWeatherByCode = getWeatherByCode;

        init();

        function init() {
            PreloadService.show();
            setIcons(vm.events);
            setEvents(vm.events);
        }

        function filterEventsByCategory(category) {
            if(!category) {
                vm.events = allEvents;
            } else {
                vm.events = allEvents.filter(function(value){
                    var found = value.category.name === category.name;
                    return !!found;
                });
            }
            vm.mapMarkers = vm.events.concat(vm.user);
        }

        function onTilesLoaded() {
            PreloadService.hide();
            SimpleScrollService.scrollTo("Events");
        }

        function setIcons(eventList) {
            var eventsLength = eventList.length;
            for(var i = 0; i < eventsLength; ++i) {
                var category = eventList[i].category.name;
                if(angular.isDefined(ICONS[category])) {
                    eventList[i].icon = ICONS[category];
                } else {
                    eventList[i].icon = ICONS.OTHER;
                }
            }
        }

        function setEvents(eventList) {
            var eventsLength = eventList.length;
            for(var i = 0; i < eventsLength; ++i) {
                var event = eventList[i];
                event.events = event.events || {};

                if(angular.isDefined(event) && event.id && event.id !== "usr") {
                    event.events.mouseover = mouseover;
                    event.events.mouseout = mouseout;
                    event.events.click = click;
                }
            }

            function mouseover(marker, eventName, object, mouseEvent) {
                vm.selectedMarker = angular.copy(object);
                vm.selectedMarker.location.latitude += 0.005;
                vm.showWindow = true;
            }

            function mouseout(marker, eventName, object, mouseEvent) {
                vm.selectedMarker = {};
                vm.showWindow = false;
            }

            function click(marker, eventName, object, mouseEvent) {
                openDetailsModal(object);
            }
        }

        function openDetailsModal(event, index) {
            var params = {
              eventId: event.id,
              coords: {
                  latitude: latitude,
                  longitude: longitude
              }
            };
            EventDetailsModalService.open(params)
                .result
                .then(updateEvent);

            function updateEvent(response) {
                event.averageRating = response.newAverageRating;
                event.favoritedCount = response.newFavoritedCount;
            }
        }

        function showOnMap(event) {
            vm.map.center.latitude = event.location.latitude;
            vm.map.center.longitude = event.location.longitude;
            SimpleScrollService.scrollTo("Events");
        }

        function convertDtToDay(dt) {
            // multiplying by 1000 to convert dt to miliseconds
            var dayIndex = new Date(dt * 1000).getDay();
            switch(dayIndex) {
                case 0: return "SUNDAY";
                case 1: return "MONDAY";
                case 2: return "TUESDAY";
                case 3: return "WEDNESDAY";
                case 4: return "THURSDAY";
                case 5: return "FRIDAY";
                case 6: return "SATURDAY";
                default: return "DATE_ERROR";
            }
        }

        function convertToCelsiusAndRound(temp) {
            return Math.round(MathService.kelvinToCelsius(temp));
        }

        function getWeatherByCode(code) {
            return WEATHER_CONDITION_CODE[code];
        }

    }
})();
