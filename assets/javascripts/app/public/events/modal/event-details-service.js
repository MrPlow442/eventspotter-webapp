(function () {
    'use strict';

    angular
        .module('app.public.events')
        .service('EventDetailsModalService', EventDetailsModalService);

    EventDetailsModalService.$inject = ['$modal'];

    function EventDetailsModalService($modal) {
        return {
            open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/public/events/modal/event-details.html",
                controller: "EventDetailsModalController as eventDetails",
                size: 'lg',
                backdrop: false,
                resolve: {
                    event: ['EventService',function(EventService) {
                        return EventService.event(params.eventId)
                            .then(function(response){
                                return response.data;
                            });
                    }],
                    coords: ['$q', function($q) {
                        var deferred = $q.defer();

                        if(angular.isDefined(params.coords)) {
                            deferred.resolve(params.coords);
                        } else {
                            console.error("params.coords is undefined");
                            deferred.reject();
                        }

                        return deferred.promise;
                    }],
                    favorited: ['AuthService', 'UserService', function(AuthService, UserService) {
                        if(AuthService.isAuthenticated()) {
                            return UserService
                                .favorited(AuthService.getUsername(), params.eventId)
                                .then(function(response){
                                   return response.data.isFavorited;
                                });
                        } else {
                            return false;
                        }
                    }]
                }
            });
        }

    }
})();