(function () {
    'use strict';

    angular
        .module('app.public.events')
        .controller('EventDetailsModalController', EventDetailsModalController);

    EventDetailsModalController.$inject = [
        '$q',
        '$modalInstance', '$timeout',
        'GMAPS_OPTIONS', 'ICONS',
        'MathService', 'AuthService', 'AlertService', 'EventService',
        'event', 'coords', 'favorited'];

    function EventDetailsModalController(
        $q,
        $modalInstance, $timeout,
        GMAPS_OPTIONS, ICONS,
        MathService, AuthService, AlertService, EventService,
        event, coords, favorited) {
        var vm = this;

        // users current location formatted for angular-google-maps markers syntax
        vm.user = {
            id: "usr",
            location: coords,
            icon: ICONS.PERSON
        };
        vm.event = angular.extend({}, event, {icon: ICONS.DESTINATION});
        vm.render = false;

        vm.map = {
            center: MathService.getMiddlePoint(event.location, vm.user.location),
            zoom: deriveZoomFromDistance(event.location, vm.user.location)
        };

        vm.mapOptions = GMAPS_OPTIONS;

        vm.markerOptions = {
            fit: false
        };

        vm.markers = [
            vm.event,
            vm.user
        ];

        vm.userRating = {
            rating: undefined,
            max: 5,
            readonly: false
        };

        vm.favorited = favorited;

        vm.dismiss = dismiss;
        vm.rate = rate;
        vm.toggleFavorite = toggleFavorite;
        vm.enableRating = AuthService.isAuthenticated;
        vm.enableFavoriteToggle = AuthService.isAuthenticated;

        init();

        function init() {
            $timeout(function() {
                vm.render = true;
            }, 500);

            $timeout(function() {
                vm.markerOptions.fit = true;
            }, 500);
        }

        function dismiss() {
            $modalInstance.close({newAverageRating: vm.event.averageRating, newFavoritedCount: vm.event.favoritedCount});
        }

        function rate(rating) {
            EventService
                .rate(vm.event.id, {value: rating})
                .then(disableRating)
                .then(reloadDetails)
                .catch(errors);

            function disableRating(response) {
                console.log("response", response);
                vm.userRating.readonly = true;
                AlertService.success({translate: "EVENT_RATING_SUCCESS"});
            }

            function reloadDetails() {
                var deferred = $q.defer();

                EventService
                    .event(vm.event.id)
                    .then(function(response){
                        angular.extend(vm.event, response.data);
                        deferred.resolve(response.data);
                    }, function(error) {
                        deferred.reject(error);
                    });

                return deferred.promise;
            }

            function errors(error) {
                console.log("error", error);
                AlertService.httpError(error);
            }
        }

        function toggleFavorite(eventId) {
            var favoriteFunc;
            if(!vm.favorited) {
                favoriteFunc = EventService.addToFavorites;
            } else {
                favoriteFunc = EventService.removeFromFavorites;
            }
            favoriteFunc(eventId)
                .then(setFavorited)
                .catch(errors);

            function setFavorited(response) {
                vm.favorited = !vm.favorited;
                if(vm.favorited) {
                    ++vm.event.favoritedCount;
                } else {
                    --vm.event.favoritedCount;
                }
                AlertService.success({translate: vm.favorited ? "EVENT_FAVORITED" : "EVENT_UNFAVORITED"});
            }

            function errors(error) {
                AlertService.httpError(error);
            }
        }

        function deriveZoomFromDistance(pos1, pos2) {
            var distance = MathService.getDistanceBetween(pos1, pos2);

            if(distance < 5 * 1000) {
                return 14;
            } else if(distance >= 5 * 1000 && distance < 10 * 1000) {
                return 12;
            } else if(distance >= 10 * 1000 && distance < 25 * 1000) {
                return 10;
            } else if(distance >= 25 * 1000 && distance < 50 * 1000) {
                return 8;
            } else {
                return 6;
            }
        }
    }
})();