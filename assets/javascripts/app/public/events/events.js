(function () {
    'use strict';

    angular
        .module('app.public.events', [
            'ui.router',
            'uiGmapgoogle-maps'
        ]);

    angular
        .module('app.public.events')
        .config(config);

    config.$inject = ['$stateProvider', 'uiGmapGoogleMapApiProvider'];

    function config($stateProvider, uiGmapGoogleMapApiProvider) {

        $stateProvider.state('events', {
            url: 'events?lat&lng',
            parent: 'public',
            views: {
                'events@app': {
                    templateUrl: 'templates/public/events/events.html',
                    controller: 'EventsController as events'
                }
            },
            resolve: {
                categories: ['CodebookService', function(CodebookService){
                    return CodebookService
                        .categories()
                        .then(function(response){
                            return response.data;
                        });
                }],
                latitude: ['$stateParams', function($stateParams){
                    return $stateParams.lat;
                }],
                longitude: ['$stateParams', function($stateParams){
                    return $stateParams.lng;
                }],
                events: ['EventService', 'latitude', 'longitude', function(EventService, latitude, longitude) {
                    var params = {
                        latitude: latitude,
                        longitude: longitude
                        //'page.size': 100,
                        //'page.offset': 0
                    };
                    return EventService
                        .events(params)
                        .then(function(response){
                            return response.data;
                        });
                }],
                weather: ['$http', 'latitude', 'longitude', function($http, latitude, longitude) {
                    var WEATHER_APPID = "5981d88d614bec16ef31631602de9035";
                    var WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast/daily";
                    var WEATHER_DAYS = 6;

                    var params = {
                        lat: latitude,
                        lon: longitude,
                        cnt: WEATHER_DAYS,
                        APPID: WEATHER_APPID
                    };

                    var config = {
                        url: WEATHER_URL,
                        method: 'GET',
                        params: params
                    };

                    return $http(config).then(function(response){
                        return response.data;
                    });
                }]
            }
        });

        // GOOGLE MAPS
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyC4wrR8G774YdsgE-8RXp9r9YQN6IydpwE',
            v: '3.17',
            libraries: 'places'
        });
    }
})();
