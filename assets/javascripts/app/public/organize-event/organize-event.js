(function () {
    'use strict';

    angular
        .module('app.public.organizeEvent', [
            'ui.router'

        ]);

    angular
        .module('app.public.organizeEvent')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {

    }
})();
