(function () {
    'use strict';

    angular
        .module('app.public.organizeEvent')
        .controller('OrganizeEventController', OrganizeEventController);

    OrganizeEventController.$inject = [
        '$q', '$filter', 'store',
        'GMAPS_OPTIONS', 'ICONS', 'DATE_FORMAT', 'TIME_FORMAT',
        'ArrayUtilService', 'GeolocationService', 'AlertService',
        'EventService',
        'categories', 'daysOfWeek'];

    function OrganizeEventController(
        $q, $filter, store,
        GMAPS_OPTIONS, ICONS, DATE_FORMAT, TIME_FORMAT,
        ArrayUtilService, GeolocationService, AlertService,
        EventService,
        categories, daysOfWeek) {
        var vm = this;

        window.orgz = this;

        vm.event = {};
        vm.categories = categories;
        vm.daysOfWeek = daysOfWeek;
        vm.selectedLanguage = null;
        vm.selectedCategory = null;
        vm.timetable = {
            startTime: undefined,
            endTime: undefined,
            days: undefined
        };

        vm.enableTimepickers = false;
        vm.coordsAcquired = false;

        vm.map = {
            center: {
                latitude: undefined,
                longitude: undefined
            },
            zoom: 10,
            marker: {
                id: 'location',
                location: {
                    latitude: undefined,
                    longitude: undefined
                }
            }
        };

        vm.mapOptions = GMAPS_OPTIONS;

        vm.markerOptions = {
            draggable: true,
            icon: ICONS.PIN
        };

        vm.languageFilter = languageFilter;
        vm.addTranslation = addTranslation;
        vm.removeTranslation = removeTranslation;
        vm.dayToggle = dayToggle;
        vm.toggleTimepickers = toggleTimepickers;
        vm.getLocationData = getLocationData;
        vm.submit = submit;
        vm.sentFormData = sentFormData;
        vm.hasLocation = hasLocation;
        vm.hasTranslations = hasTranslations;

        function submit() {
            save()
                .then(hideForm)
                .catch(errors);

            function hideForm(result) {
                sentFormData(true);
            }

            function errors(error) {
                if(error.validation) {
                    AlertService.danger({translate: error.validation});
                } else if(error.http) {
                    AlertService.httpError(error.http);
                }
            }
        }

        function save() {
            var deferred = $q.defer();

            var location = angular.extend({}, vm.map.marker.location);
            var timetable = vm.enableTimepickers ? angular.extend({}, vm.timetable) : null;
            var data = angular.extend({}, vm.event, {location: location}, {timetable: timetable});
            data.startDate = $filter('date')(data.startDate, DATE_FORMAT.modelDateFormat);
            data.endDate = $filter('date')(data.endDate, DATE_FORMAT.modelDateFormat);

            if(data.timetable) {
                data.timetable.startTime = $filter('date')(data.timetable.startTime, TIME_FORMAT.modelTimeFormat);
                data.timetable.endTime = $filter('date')(data.timetable.startTime, TIME_FORMAT.modelTimeFormat);
            }

            console.log(data);
            console.log(JSON.stringify(data));

            if(!location.latitude || !location.longitude) {
                deferred.reject({validation:'ORGANIZE_EVENT_ERROR_LOCATION_MISSING'});
            }

            if(angular.isUndefined(data.translations) ||
                (angular.isDefined(data.translations) && data.translations.length === 0)) {
                deferred.reject({validation:'ORGANIZE_EVENT_ERROR_TRANSLATIONS_MISSING'});
            }

            EventService
                .create(data)
                .then(proceed)
                .catch(httpError);

            function proceed(response) {
                deferred.resolve(response);
            }

            function httpError(error) {
                deferred.reject({http: error});
            }

            return deferred.promise;
        }

        function sentFormData(isSent) {
            if(angular.isDefined(isSent)) {
                store.set('eventFormDataSent', isSent);
            }
            return !!store.get('eventFormDataSent');
        }

        function languageFilter(element) {
            if(!vm.event.translations) {
                return true;
            }

            var translationsLength = vm.event.translations.length;
            for(var i = 0; i < translationsLength; ++i) {
                var translation = vm.event.translations[i];
                if(element.locale === translation.language.locale) {
                    return false;
                }
            }

            return true;
        }

        function addTranslation(language) {
            if(!angular.isArray(vm.event.translations)) {
                vm.event.translations = [];
            }

            var translation = {
                language: language
            };

            vm.selectedLanguage = null;
            vm.event.translations.push(translation);
        }

        function removeTranslation(translation) {
            var index = ArrayUtilService.indexOfByProperties(vm.event.translations, translation, "language.locale");

            if(index === -1) {
                return;
            }

            vm.event.translations.splice(index, 1);
        }

        function dayToggle(selection) {
            if(angular.isUndefined(vm.timetable.days)) {
                vm.timetable.days = [];
            }

            var index = ArrayUtilService.angularIndexOf(vm.timetable.days, selection.name);
            if(index === -1) {
                ArrayUtilService.pushUnique(vm.timetable.days, selection.name);
            } else {
                vm.timetable.days.splice(index, 1);
            }
        }

        function toggleTimepickers() {
            vm.enableTimepickers = !vm.enableTimepickers;
        }

        function getLocationData(options) {
            GeolocationService
                .getCurrentPosition(options)
                .then(assignCoords)
                .catch(errors);

            //////
            function assignCoords(response) {
                var coords = {
                    latitude: response.coords.latitude,
                    longitude: response.coords.longitude
                };

                angular.copy(coords, vm.map.center);
                angular.copy(coords, vm.map.marker.location);
                vm.coordsAcquired = true;
            }

            function errors(error) {
                console.error(error);
            }
        }

        function hasLocation() {
            return vm.coordsAcquired;
        }

        function hasTranslations() {
            return angular.isDefined(vm.event.translations) && vm.event.translations.length > 0;
        }

    }
})();
