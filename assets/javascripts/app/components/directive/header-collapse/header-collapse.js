(function () {
    'use strict';

    angular
        .module('app.components.directive')
        .directive('headerCollapse', headerCollapse);

    headerCollapse.$inject = ['$window'];

    function headerCollapse($window) {
        return {
            link: link,
            restrict: 'A',
            scope: {

            }
        };

        function link(scope, element, attrs) {
            angular.element($window).on('scroll', collapseHeader);

            function collapseHeader() {
                if(getOffsetOf(element[0]).top > 50) {
                    element.addClass("top-nav-collapse");
                } else {
                    element.removeClass("top-nav-collapse");
                }
            }
        }

        function getOffsetOf(element) {
            //stolen from jquery offset function, and reworked to be a standalone function instead of a member function
            var docElem, win, elem = element,
                box = {
                    top: 0,
                    left: 0
                },
                doc = elem && elem.ownerDocument;

            if (!doc) {
                return;
            }

            docElem = doc.documentElement;

            // Make sure it's not a disconnected DOM node
            if (!(docElem !== elem && docElem.contains(elem))) {
                return box;
            }

            if (angular.isDefined(elem.getBoundingClientRect)) {
                box = elem.getBoundingClientRect();
            }
            win = $window;
            return {
                top: box.top + win.pageYOffset - docElem.clientTop,
                left: box.left + win.pageXOffset - docElem.clientLeft
            };
        }
    }

})();
