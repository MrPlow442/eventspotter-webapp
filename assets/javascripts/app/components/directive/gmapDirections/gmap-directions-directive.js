(function () {
    'use strict';

    angular
        .module('app.components.directive')
        .directive('gmapDirections', gmapDirections);

    gmapDirections.$inject = [];

    function gmapDirections() {
        return {
            link: link,
            restrict: 'E',
            priority: -1,
            require: '^uiGmapGoogleMap',
            scope: {
                from: '=',
                to: '='
            }
        };

        function link(scope, element, attrs, mapCtrl) {
            return mapCtrl.getScope()
                .deferred
                .promise
                .then(function(map) {
                    var poly = new google.maps.Polyline({ map: map});
                    var path = new google.maps.MVCArray();
                    var service = new google.maps.DirectionsService();

                    var origin = new google.maps.LatLng(scope.from.latitude, scope.from.longitude);
                    var dest = new google.maps.LatLng(scope.to.latitude, scope.to.longitude);

                    var routeParams = {
                        origin: origin,
                        destination: dest,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    };

                    service.route(routeParams, routeCallback);

                    function routeCallback(result, status) {
                        if(status == google.maps.DirectionsStatus.OK) {
                            var len = result.routes[0].overview_path.length;
                            for(var i = 0; i < len; i++) {
                                path.push(result.routes[0].overview_path[i]);
                            }

                            poly.setPath(path);
                        }
                    }
            });
        }
    }

})();