(function () {
    'use strict';

    angular
        .module('app.components.directive')
        .directive('confirmPassword', confirmPassword);

    confirmPassword.$inject = [];

    function confirmPassword() {
        return {
            link: link,
            restrict: 'A',
            require: 'ngModel',
            scope: {
                otherPassword: "=confirmPassword",
                disableConfirm: "="
            }
        };

        function link(scope, element, attrs, ngModel) {
            ngModel.$validators.confirmPassword = confirmPasswordValidator;
            scope.$watch("otherPassword", watchOtherPassword);

            function confirmPasswordValidator(modelValue) {
                if(scope.disableConfirm) {
                    return true;
                } else {
                    return modelValue === scope.otherPassword;
                }
            }

            function watchOtherPassword() {
                ngModel.$validate();
            }
        }
    }

})();