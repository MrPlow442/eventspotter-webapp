(function () {
    'use strict';

    angular
        .module('app.components.directive')
        .directive('loginComponent', loginComponent);

    loginComponent.$inject = ['AuthService', 'AlertService'];

    function loginComponent(AuthService, AlertService) {
        return {
            link: link,
            restrict: 'E',
            templateUrl: 'templates/components/auth/login.html',
            scope: {
                cancel: '&?',
                done: '&?'
            }
        };

        function link(scope, element, attrs) {
            var vm = scope;

            vm.login = login ;
            vm.cancel = vm.cancel || angular.noop;
            vm.done = vm.done || angular.noop;

            vm.formData = {
                username: null,
                password: null
            };

            function login() {
                AuthService.login(vm.formData.username, vm.formData.password)
                    .then(onLoginSuccess, onLoginError);

                function onLoginSuccess(success) {
                    console.log(success);
                    vm.done({success: true});
                }

                function onLoginError(error) {
                    console.log(error);
                    AlertService.push({translate: 'LOGIN_ERROR'});
                    vm.done({success: false});
                }
            }

        }
    }

})();