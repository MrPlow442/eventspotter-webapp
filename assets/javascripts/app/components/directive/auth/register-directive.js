(function () {
    'use strict';

    angular
        .module('app.components.directive')
        .directive('registerComponent', registerComponent);

    registerComponent.$inject = ['UserService', 'AlertService'];

    function registerComponent(UserService, AlertService) {
        return {
            link: link,
            restrict: 'E',
            templateUrl: 'templates/components/auth/register.html',
            scope: {
                cancel: '&?',
                done: '&?'
            }
        };

        function link(scope, element, attrs) {
            var vm = scope;

            vm.register = register;
            vm.cancel = vm.cancel || angular.noop;
            vm.done = vm.done || angular.noop;

            vm.formData = {
                username: undefined,
                password: undefined,
                email: undefined,
                showEmail: true,
                phoneNumber: undefined,
                firstName: undefined,
                lastNameOrTitle: undefined
            };

            vm.otherData = {
                confirmPassword: undefined
            };

            function register() {
                UserService.register(vm.formData)
                    .then(onRegisterSuccess, onRegisterError);

                function onRegisterSuccess(success) {
                    console.log(success);
                    AlertService.success({translate: "REGISTER_SUCCESS"});
                    vm.done({success: true});
                }

                function onRegisterError(error) {
                    console.log(error);
                    AlertService.httpError(error);
                    vm.done({success: false});
                }
            }

        }
    }

})();