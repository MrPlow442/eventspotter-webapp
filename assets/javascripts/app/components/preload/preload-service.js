(function () {
    'use strict';

    angular
        .module('preload')
        .service('PreloadService', PreloadService);

    PreloadService.$inject = [];

    function PreloadService() {
        var _show = false;

        return {
            show: show,
            hide: hide,
            isShow: isShow
        };

        function show() {
            _show = true;
        }

        function hide() {
            _show = false;
        }

        function isShow() {
            return _show;
        }
    }
})();