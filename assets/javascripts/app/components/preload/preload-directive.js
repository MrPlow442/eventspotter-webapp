(function () {
    'use strict';

    angular
        .module('preload')
        .directive('preload', preload);

    preload.$inject = ['PreloadService', '$q', '$http', '$timeout'];

    function preload(PreloadService, $q, $http, $timeout) {
        return {
            link: link,
            restrict: 'E',
            templateUrl: 'templates/components/preload/preload.html',
            scope: {
                show: '=?'
            }
        };

        function link(scope, element, attrs) {

            var timeoutHandler = undefined;

            scope.showPreload = showPreload;

            scope.show = angular.isDefined(scope.show) ? scope.show : false;
            scope.httpShow = false;

            function showPreload() {
                httpCheck();
                return scope.show || scope.httpShow || PreloadService.isShow();
            }

            function httpCheck() {
                var hasPendingRequests = $http.pendingRequests.length > 0;
                if(hasPendingRequests) {
                    if(angular.isDefined(timeoutHandler)) {
                        if(timeoutHandler.$$state.status === 0) {
                            return;
                        } else {
                            cleanup();
                        }
                    }
                    timeoutHandler = $timeout(function(){
                        hasPendingRequests = $http.pendingRequests.length > 0;
                        scope.httpShow = !!hasPendingRequests;
                    }, 100);
                } else {
                    scope.httpShow = false;
                }
            }

            function cleanup() {
                if(angular.isDefined(timeoutHandler)) {
                    $timeout.cancel(timeoutHandler);
                    timeoutHandler = undefined;
                }
            }

            scope.$on('$destroy', function(){
               cleanup();
            });

        }
    }

})();