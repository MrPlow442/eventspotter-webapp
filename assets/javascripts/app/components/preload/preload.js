(function () {
    'use strict';

    angular
        .module('preload', []);

    angular
        .module('preload')
        .config(config);

    config.$inject = ['$httpProvider'];

    function config($httpProvider) {
        //TODO: add XHR interceptor so that the loading bar shows on XHR requests as well
    }
})();