(function () {
    'use strict';

    angular
        .module('app.constants', []);

    angular.module('app.constants')
        .constant('DEFAULT_LOCALE', 'hr_HR')
        .constant('DATE_FORMAT', {
            format: "dd.MM.yyyy.",
            placeholder: "DD.MM.GGGG.",
            modelDateFormat: "yyyy-MM-dd"
        })
        .constant('TIME_FORMAT', {
            modelTimeFormat: "HH:mm"
        })
        .constant('USER_ROLE', {
            ROLE_USER: "ROLE_USER",
            ROLE_ADMIN: "ROLE_ADMIN",
            ROLE_SUPER_ADMIN: "ROLE_SUPER_ADMIN"
        })
        .constant('SERVICE_URL', 'http://localhost:8080/EventSpotter')
        .constant('GMAPS_OPTIONS', {
            scrollwheel: false,
            styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
        })
        .constant('ICONS', {
            ARCHITECTURE: 'images/markers/architecture.png',
            SPORTS: 'images/markers/sports.png',
            ENTERTAINMENT: 'images/markers/entertainment.png',
            RELIGION: 'images/markers/religion.png',
            RELAXATION: 'images/markers/relaxation.png',
            FOOD_AND_DRINKS: 'images/markers/food.png',
            OTHER: 'images/markers/other.png',
            PIN: 'images/markers/pin.png',
            PERSON: 'images/markers/person.png',
            PERSON_FOCUS: 'images/markers/person_focus.png',
            DESTINATION: 'images/markers/destination.png'
        })
        .constant('WEATHER_CONDITION_CODE', {
            200: "THUNDERSTORM_WITH_LIGHT_RAIN",
            201: "THUNDERSTORM_WITH_RAIN",
            202: "THUNDERSTORM_WITH_HEAVY_RAIN",
            210: "LIGHT_THUNDERSTORM",
            211: "THUNDERSTORM",
            212: "HEAVY_THUNDERSTORM",
            221: "RAGGED_THUNDERSTORM",
            230: "THUNDERSTORM_WITH_LIGHT_DRIZZLE",
            231: "THUNDERSTORM_WITH_DRIZZLE",
            232: "THUNDERSTORM_WITH_HEAVY_DRIZZLE",
            300: "LIGHT_INTENSITY_DRIZZLE",
            301: "DRIZZLE",
            302: "HEAVY_INTENSITY_DRIZZLE",
            310: "LIGHT_INTENSITY_DRIZZLE RAIN",
            311: "DRIZZLE_RAIN",
            312: "HEAVY_INTENSITY_DRIZZLE_RAIN",
            313: "SHOWER_RAIN_AND_DRIZZLE",
            314: "HEAVY_SHOWER_RAIN_AND_DRIZZLE",
            321: "SHOWER_DRIZZLE",
            500: "LIGHT_RAIN",
            501: "MODERATE_RAIN",
            502: "HEAVY_INTENSITY_RAIN",
            503: "VERY_HEAVY_RAIN",
            504: "EXTREME_RAIN",
            511: "FREEZING_RAIN",
            520: "LIGHT_INTENSITY_SHOWER_RAIN",
            521: "SHOWER_RAIN",
            522: "HEAVY_INTENSITY_SHOWER_RAIN",
            531: "RAGGED_SHOWER_RAIN",
            600: "LIGHT_SNOW",
            601: "SNOW",
            602: "HEAVY_SNOW",
            611: "SLEET",
            612: "SHOWER_SLEET",
            615: "LIGHT_RAIN_AND_SNOW",
            616: "RAIN_AND_SNOW",
            620: "LIGHT_SHOWER_SNOW",
            621: "SHOWER_SNOW",
            622: "HEAVY_SHOWER_SNOW",
            701: "MIST",
            711: "SMOKE",
            721: "HAZE",
            731: "SAND_DUST_WHIRLS",
            741: "FOG",
            751: "SAND",
            761: "DUST",
            762: "VOLCANIC_ASH",
            771: "SQUALLS",
            781: "TORNADO",
            800: "CLEAR_SKY",
            801: "FEW_CLOUDS",
            802: "SCATTERED_CLOUDS",
            803: "BROKEN_CLOUDS",
            804: "OVERCAST_CLOUDS",
            900: "TORNADO",
            901: "TROPICAL_STORM",
            902: "HURRICANE",
            903: "COLD",
            904: "HOT",
            905: "WINDY",
            906: "HAIL",
            951: "CALM",
            952: "LIGHT_BREEZE",
            953: "GENTLE_BREEZE",
            954: "MODERATE_BREEZE",
            955: "FRESH_BREEZE",
            956: "STRONG_BREEZE",
            957: "HIGH_WIND_NEAR_GALE",
            958: "GALE",
            959: "SEVERE_GALE",
            960: "STORM",
            961: "VIOLENT_STORM",
            962: "HURRICANE"
        });
})();
