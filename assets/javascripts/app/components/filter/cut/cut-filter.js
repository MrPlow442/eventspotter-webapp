(function () {
    'use strict';

    /**
     * Text truncation filter, lifted from: http://stackoverflow.com/questions/18095727/limit-the-length-of-a-string-with-angularjs
     */

    angular
        .module('app.components.filter')
        .filter('cut', cut);

    cut.$inject = [];

    function cut() {
        return cutFilter;

        function cutFilter(value, cutByWords, max, tail) {
            if(!value) return '';

            max = parseInt(max, 10);
            if(!max) return value;
            if(value.length <= max) return value;

            value = value.substr(0, max);
            if(cutByWords) {
                var lastSpace = value.lastIndexOf(' ');
                if(lastSpace != -1) {
                    value = value.substr(0, lastSpace);
                }
            }

            return value + (tail || '...');
        }
    }

})();
