(function () {
    'use strict';

    angular
        .module('app.components.filter')
        .filter('paginationOffset', PaginationOffsetFilter);

    PaginationOffsetFilter.$inject = [];

    function PaginationOffsetFilter() {
        return function(input, start) {
            start = parseInt(start, 10);
            return angular.isArray(input) ? input.slice(start) : input;
        };
    }
})();
