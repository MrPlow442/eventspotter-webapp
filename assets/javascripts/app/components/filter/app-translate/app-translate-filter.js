(function() {
    'use strict';

    angular
        .module('app.components.filter')
        .filter('appTranslate', appTranslate);

    appTranslate.$inject = ['$translate', 'DEFAULT_LOCALE']

    function appTranslate($translate, DEFAULT_LOCALE) {
        return appTranslateFilter;

        function appTranslateFilter(item, property) {

            var itemDoesntExist = angular.isUndefined(item) || angular.isUndefined(item.translations);
            var translationsIsntArray = !itemDoesntExist && (angular.isDefined(item.translations) && !angular.isArray(item.translations));
            var arrayIsEmpty = !itemDoesntExist && !translationsIsntArray && item.translations.length === 0;
            if(itemDoesntExist || translationsIsntArray || arrayIsEmpty) {
                return item;
            }

            var currentLocale = $translate.use();

            var translation = findTranslationIn(item.translations, currentLocale);

            if(angular.isUndefined(translation)){
                translation = findTranslationIn(item.translations, DEFAULT_LOCALE);
            }

            if(angular.isUndefined(translation)) {
                translation = item.translations[0];
            }

            if(angular.isDefined(property) && angular.isString(property)) {
                return translation[property];
            }

            return translation;
        }

        function findTranslationIn(translations, locale) {
            var length = translations.length;
            for(var i = 0; i < length; ++i) {
                var translation = translations[i];
                if(translation.language.locale === locale) {
                    return translation;
                }
            }
            return undefined;
        }
    }

})();