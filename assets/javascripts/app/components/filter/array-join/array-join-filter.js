(function () {
    'use strict';

    angular
        .module('app.components.filter')
        .filter('arrayJoin', arrayJoin);

    arrayJoin.$inject = [];

    function arrayJoin() {
        return arrayJoinFilter;

        function arrayJoinFilter(array, separator, property) {
            console.log("array", array);
            if(!angular.isArray(array)) {
                return array;
            }

            var items;

            if(property) {
                items = array.map(function(value, index, array) {
                   return value[property]
                });
            } else {
                items = array;
            }

            separator = separator || ", ";

            return items.join(separator);
        }
    }

})();