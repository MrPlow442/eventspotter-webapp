(function () {
    'use strict';

    angular
        .module('app.components.filter')
        .filter('templateValues', templateValues);

    templateValues.$inject = [];

    function templateValues() {
        return templateValuesFilter;

        function templateValuesFilter() {
            if(!angular.isString(arguments[0])) {
                return arguments[0];
            }

            var str = arguments[0];

            for(var i = 1; i < arguments.length; ++i) {
                //NOTHING TO REPEAT ERRROR
                var pattern = "\\{" + (i-1) + "\\}";
                str = str.replace(new RegExp(pattern, 'g'), arguments[i]);
            }
            return str;
        }
    }

})();