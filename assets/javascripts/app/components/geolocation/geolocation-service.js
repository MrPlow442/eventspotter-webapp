(function(){
    'use strict';

    angular
        .module('geolocation')
        .service('GeolocationService', GeolocationService);

    GeolocationService.$inject = ['$window', '$q'];

    function GeolocationService($window, $q) {
        var service = {
            // these two objects represent the return value so that the user doesn't have to call value method of deferred objects
            position: {},
            error: {},
            isGeolocationSupported: isGeolocationSupported,
            getCurrentPosition: getCurrentPosition
        };

        return service;

        ////////////////

        function isGeolocationSupported() {
            return "geolocation" in $window.navigator && typeof $window.navigator.geolocation !== "undefined";
        }

        function getCurrentPosition(positionOptions) {
            var deferred = $q.defer();

            if(isGeolocationSupported()) {
                $window.navigator.geolocation.getCurrentPosition(successCallback, errorCallback, positionOptions);
            } else {
                //TODO: inject geolocation script for older browsers in case HTML5 geolocation is not supported
                deferred.reject({error: {code: 4, message: "Current browser does not support geolocation service"}});
            }

            return deferred.promise;

            // A callback which is called by getCurrentPosition when position is succesfully retrieved
            function successCallback(position) {
                service.position = position;
                deferred.resolve(position);
            }

            // A callback which is called by getCurrentPosition when a position could not be retrieved
            function errorCallback(error) {
                var errorObject = {error: error};
                service.error = errorObject;
                deferred.reject(errorObject);
            }

        }
    }
}());