(function() {
    'use strict';

    angular
        .module('app.components.modal')
        .controller('MyFavoritesModalController', MyFavoritesModalController);

    MyFavoritesModalController.$inject = [
        '$modalInstance',
        'EventService', 'AlertService', 'ArrayUtilService', 'GeolocationService', 'EventDetailsModalService',
        'events'];

    function MyFavoritesModalController(
        $modalInstance,
        EventService, AlertService, ArrayUtilService, GeolocationService, EventDetailsModalService,
        events) {
        var vm = this;

        // public methods
        vm.events = events;

        vm.pagination = {
            size: 5,
            events: 1
        };

        vm.paginationOptions = {
            maxSize: 10,
            boundaryLinks: true,
            rotate: false
        };

        console.log(events);

        vm.openDetailsModal = openDetailsModal;
        vm.removeFromFavorites = removeFromFavorites;
        vm.confirm = confirm;
        vm.cancel = cancel;

        // private methods
        function confirm() {
            $modalInstance.close();
        }

        function cancel() {
            $modalInstance.dismiss();
        }

        function openDetailsModal(event) {

            GeolocationService
                .getCurrentPosition()
                .then(showDetails)
                .catch(errors);


            function showDetails(response) {
                var params = {
                    eventId: event.id,
                    coords: {
                        latitude: response.coords.latitude,
                        longitude: response.coords.longitude
                    }
                };
                EventDetailsModalService.open(params);
            }

            function errors(error) {
                console.log(error);
            }
        }

        function removeFromFavorites(event) {
             EventService
                 .removeFromFavorites(event.id)
                 .then(success)
                 .catch(error);

            function success(response) {
                ArrayUtilService.removeIfExists(vm.events, event);
                AlertService.success({translate: "EVENT_UNFAVORITED"});
                $modalInstance.dismiss();
            }

            function error(error) {
                AlertService.httpError(error);
            }
        }
    }
})();