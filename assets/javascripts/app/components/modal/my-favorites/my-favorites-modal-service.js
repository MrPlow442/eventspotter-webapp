(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .service('MyFavoritesModalService', MyFavoritesModalService);

    MyFavoritesModalService.$inject = ['$modal'];

    function MyFavoritesModalService($modal) {
        return {
            open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/components/modal/my-favorites/my-favorites-modal.html",
                controller: "MyFavoritesModalController as myFavoritesModal",
                size: 'lg',
                backdrop: false,
                resolve: {
                    events: ['$q', 'AuthService', 'UserService', function($q, AuthService, UserService) {
                        var deferred = $q.defer();

                        if(!AuthService.isAuthenticated()) {
                            deferred.reject("User not authenticated");
                        }

                        UserService.favorites(AuthService.getUsername())
                            .then(function(response){
                                deferred.resolve(response.data);
                            });

                        return deferred.promise;
                    }]
                }
            });
        }
    }
})();