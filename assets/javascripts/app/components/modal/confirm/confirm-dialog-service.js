(function() {
    'use strict';

    angular
        .module('app.components.modal')
        .service('ConfirmDialogService', ConfirmDialogService);

    ConfirmDialogService.$inject = ['$modal'];

    function ConfirmDialogService($modal) {
        return {
          open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/components/modal/confirm/confirm-dialog.html",
                controller: "ConfirmDialogController as confirmDialog",
                size: 'lg',
                backdrop: false,
                resolve: {
                }
            });
        }
    }
})();
