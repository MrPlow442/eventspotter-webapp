(function() {
    'use strict';

    angular
        .module('app.components.modal')
        .controller('ConfirmDialogController', ConfirmDialogController);

    ConfirmDialogController.$inject = ['$modalInstance'];

    function ConfirmDialogController($modalInstance) {
        var vm = this;

        // public methods

        vm.confirm = confirm;
        vm.cancel = cancel;

        // private methods
        function confirm() {
            $modalInstance.close();
        }

        function cancel() {
            $modalInstance.dismiss();
        }
    }
})();