(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .controller('AuthModalController', AuthModalController);

    AuthModalController.$inject = ['$modalInstance', 'AuthService', 'AlertService'];

    function AuthModalController($modalInstance, AuthService, AlertService) {
        var vm = this;

        var TABS = {
          LOGIN: "login",
          REGISTER: "register"
        };

        vm.tab = {
            login: { active: true },
            register: { actove: false }
        };

        vm.TABS = TABS;

        vm.activateTab = activateTab;
        vm.close = close;

        function activateTab(tab) {
            if(!angular.isString(tab)) {
                return;
            }
            if(tab === TABS.LOGIN && !vm.tab.login.active) {
                vm.tab.register.active = false;
                vm.tab.login.active = true;
            }
            if(tab === TABS.REGISTER && !vm.tab.register.active) {
                vm.tab.login.active = false;
                vm.tab.register.active = true;
            }
        }

        function close(data) {
            console.log("close called with data", data);
            if(data) {
                $modalInstance.close(data);
            }
            $modalInstance.dismiss();
        }
    }
})();
