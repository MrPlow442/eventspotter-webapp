(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .factory('AuthModalService', AuthModalService);

    AuthModalService.$inject = ['$modal'];

    function AuthModalService($modal) {
        return {
            open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/components/modal/auth/layout.html",
                controller: "AuthModalController as authModal",
                size: 'lg',
                backdrop: false,
                resolve: {
                }
            });
        }
    }
})();
