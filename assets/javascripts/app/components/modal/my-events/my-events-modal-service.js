(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .service('MyEventsModalService', MyEventsModalService);

    MyEventsModalService.$inject = ['$modal'];

    function MyEventsModalService($modal) {
        return {
            open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/components/modal/my-events/my-events-modal.html",
                controller: "MyEventsModalController as myEventsModal",
                size: 'lg',
                backdrop: false,
                resolve: {
                    events: ['$q', 'AuthService', 'UserService', function($q, AuthService, UserService) {
                        var deferred = $q.defer();

                        if(!AuthService.isAuthenticated()) {
                            deferred.reject("User not authenticated");
                        }

                        UserService.events(AuthService.getUsername())
                            .then(function(response){
                                deferred.resolve(response.data);
                            });

                        return deferred.promise;
                    }]
                }
            });
        }
    }
})();