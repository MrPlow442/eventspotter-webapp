(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .factory('LoginModalService', LoginModalService);

    LoginModalService.$inject = ['$modal'];

    function LoginModalService($modal) {
        return {
            open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/components/modal/auth/layout.html",
                controller: "LoginModalController as loginModal",
                size: 'lg',
                backdrop: false,
                resolve: {
                }
            });
        }
    }
})();
