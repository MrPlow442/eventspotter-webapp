(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .controller('LoginModalController', LoginModalController);

    LoginModalController.$inject = ['$modalInstance', 'AuthService', 'AlertService'];

    function LoginModalController($modalInstance, AuthService, AlertService) {
        var vm = this;

        vm.login = login ;
        vm.cancel = cancel;

        vm.formData = {
            username: null,
            password: null
        };

        function login() {
            AuthService.login(vm.formData.username, vm.formData.password)
                .then(onLoginSuccess, onLoginError);

            function onLoginSuccess(success) {
                console.log(success);
                $modalInstance.close({success: true});
            }

            function onLoginError(error) {
                console.log(error);
                AlertService.push({translate: 'ALERT_LOGIN_ERROR'});
                $modalInstance.close({success: false});
            }
        }

        function cancel() {
            $modalInstance.dismiss();
        }

    }
})();
