(function() {
    'use strict';

    angular
        .module('app.components.modal')
        .controller('ProfileModalController', ProfileModalController);

    ProfileModalController.$inject = ['$modalInstance', 'UserService', 'AlertService', 'user'];

    function ProfileModalController($modalInstance, UserService, AlertService, user) {
        var vm = this;

        window.prfModal = this;

        // public methods
        vm.user = user;

        console.log(user);

        vm.update = update;
        vm.confirm = confirm;
        vm.cancel = cancel;

        // private methods
        function confirm() {
            $modalInstance.close();
        }

        function cancel() {
            $modalInstance.dismiss();
        }

        function update() {
            UserService.update(vm.user.username, vm.user)
                .then(onSuccess, onError);

            function onSuccess(response) {
                console.log(response);
                AlertService.push({type: "success", translate:"USER_PROFILE_UPDATE_SUCCESS"});
            }

            function onError(error) {
                console.error(error);
                AlertService.httpError(error);
            }
        }
    }
})();