(function () {
    'use strict';

    angular
        .module('app.components.modal')
        .service('ProfileModalService', ProfileModalService);

    ProfileModalService.$inject = ['$modal'];

    function ProfileModalService($modal) {
        return {
            open: open
        };

        function open(params) {
            return $modal.open({
                templateUrl: "templates/components/modal/profile/profile-modal.html",
                controller: "ProfileModalController as profileModal",
                size: 'lg',
                backdrop: false,
                resolve: {
                    user: ['$q', 'AuthService', 'UserService', function($q, AuthService, UserService){
                        var deferred = $q.defer();

                        if(!AuthService.isAuthenticated()) {
                            deferred.reject("User not authenticated");
                        }

                        UserService.user(AuthService.getUsername())
                            .then(function(response){
                               deferred.resolve(response.data);
                            });

                        return deferred.promise;
                    }]
                }
            });
        }
    }
})();