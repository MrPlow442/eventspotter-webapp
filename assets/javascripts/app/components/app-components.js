(function () {
    'use strict';

    angular
        .module('app.components', [
            'app.components.auth',
            'app.components.modal',
            'app.components.service',
            'app.components.filter',
            'app.components.directive',
            'app.constants'
        ]);
})();
