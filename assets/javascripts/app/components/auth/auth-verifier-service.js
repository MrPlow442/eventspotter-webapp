(function () {
    'use strict';

    angular
        .module('app.components.auth')
        .service('AuthVerifierService', AuthVerifierService);

    AuthVerifierService.$inject = ['$interval' ,'AuthService'];

    function AuthVerifierService($interval ,AuthService) {
        var DEFAULT_CONFIG = {
            duration: 300000 // 5 min
        };

        var verifier = undefined;
        var lastKnownState = undefined;

        return {
            startVerifier: startVerifier,
            stopVerifier: stopVerifier,
            getLastKnownState: getLastKnownState
        };

        function startVerifier(config) {
            config = config || DEFAULT_CONFIG;

            // first verify, then start interval
            verify();
            verifier = $interval(verify, config.duration);

            function verify() {
                AuthService
                    .validate()
                    .then(success, error);
            }

            function success(state) {
                setLastKnownState(state);
            }

            function error(state) {
                setLastKnownState(state);
            }

            function setLastKnownState(state) {
                lastKnownState = state;
            }
        }

        function stopVerifier() {
            if(!verifier) {
                $interval.cancel(verifier);
                verifier = undefined;
            }
        }

        function getLastKnownState() {
            return lastKnownState;
        }
    }
})();