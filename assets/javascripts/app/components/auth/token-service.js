(function(){
    angular
        .module('app.components.auth')
        .service('TokenService', TokenService);

    TokenService.$inject = ['$http', '$q', 'store', 'SERVICE_URL'];

    function TokenService($http, $q, store, SERVICE_URL) {
        var _token = null;

        return {
            set: set,
            get: get,
            clear: clear
        };

        function set(token) {
            _token = token;
            store.set('token', token);
            return _token;
        }

        function get() {
            if(!_token) {
                _token = store.get('token');
            }
            return _token;
        }

        function clear() {
            if(_token) {
                _token = null;
                store.set('token', null);
            }
        }
    }
})();
