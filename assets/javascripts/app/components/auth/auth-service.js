(function(){
    'use strict';

    angular
        .module('app.components.auth')
        .service('AuthService', AuthService);

    AuthService.$inject = ['$http', '$q', 'TokenService', 'SERVICE_URL'];

    function AuthService($http, $q, TokenService, SERVICE_URL) {
        return {
            login: login,
            logout: logout,
            isAuthenticated: isAuthenticated,
            getUsername: getUsername,
            getRoles: getRoles,
            hasRoles: hasRoles,
            validate: validate
        };

        function login(username, password) {
            var config = {
                url: SERVICE_URL + '/api/login',
                method: 'POST',
                data: {
                    username: username,
                    password: password
                }
            };

            var deferred = $q.defer();

            $http(config)
                .then(function(response){
                    var token = response.data;
                    TokenService.set(token);
                    deferred.resolve(token);
                }, function(error){
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function logout(token) {
            var config = {
                url: SERVICE_URL + '/api/logout',
                method: 'POST',
                headers: {
                 'Authorization': token.token_type + " " + token.access_token
                }
            };

            var deferred = $q.defer();

            $http(config)
                .then(function(response){
                   console.log(response);
                   TokenService.clear();
                }, function(error){
                    console.log(error);
                    if(error.status == 404) {
                        TokenService.clear();
                    }
                });

            return deferred.promise;
        }

        function isAuthenticated() {
            return TokenService.get() != null;
        }

        function validate(token) {
            token = token || TokenService.get();

            var deferred = $q.defer();

            if(!token) {
                deferred.reject({valid: false, token: undefined});
            } else {
                var config = {
                    url: SERVICE_URL + '/api/validate',
                    method: 'POST',
                    headers: {
                        'Authorization': token.token_type + " " + token.access_token
                    }
                };

                $http(config)
                    .then(function(response){
                        deferred.resolve({valid: true, token: response.data});
                    }, function(error){
                        TokenService.clear();
                        deferred.resolve({valid: false, token: undefined});
                    });
            }


            return deferred.promise;
        }

        function getUsername() {
            var token = TokenService.get();
            return token ? token.username : null;
        }

        /**
         * @returns an array containing strings which represent user roles
         */
        function getRoles() {
            var token = TokenService.get();
            return token ? token.roles : null;
        }

        function hasRoles(roles) {
            var userRoles = getRoles();

            if(userRoles === null) {
                return false;
            }

            if(angular.isArray(roles)) {
                return containsAny(getRoles(), roles);
            }
            if(angular.isString(roles)) {
                return getRoles().indexOf(roles) != -1;
            }
        }

        /**
         * @param source
         * @param target
         * @returns {boolean}
         */
        function containsAny(source, target) {
            if(angular.isUndefined(source) || angular.isUndefined(target)) {
                return false;
            }
            var result = source.filter(function(item){ return target.indexOf(item) > -1 });
            return (result.length > 0);
        }
    }

})();
