(function () {
    'use strict';

    angular
        .module('simpleScroll')
        .service('SimpleScrollService', SimpleScrollService);

    SimpleScrollService.$inject = ['$q', '$window', '$document', '$interval', 'MathUtilService'];

    function SimpleScrollService($q, $window, $document, $interval, MathUtilService) {
        var _targets = [];
        return {
            registerTarget: registerTarget,
            scrollTo: scrollTo
        };

        function registerTarget(name, target) {
            if(angular.isUndefined(name) && angular.isUndefined(target)){
                throw "Invalid parameters";
            }
            if(!angular.isElement(target) && !angular.isNumber(target)) {
                throw "Target is neither an element nor a number"
            }

            if(target instanceof jQuery) {
                _targets.push({name: name, target: target[0]});
            } else {
                _targets.push({name: name, target: target});
            }

        }

        function scrollTo(targetName) {
            var target = _targets.find(byTargetName);
            if(angular.isUndefined(target)) {
                return;
            }

            return scrollToElement($document[0].documentElement, target.target, 2000);

            ////////////
            function byTargetName(item, index, list) {
                return item.name == targetName;
            }
        }

        function scrollToElement(element, target, duration) {
            var deferred = $q.defer();

            var targetValue = undefined;
            if(angular.isElement(target)) {
                targetValue = calculateElementTop(target);
            } else if(angular.isNumber(target)) {
                targetValue = target;
            }

            if(angular.isUndefined(targetValue)) {
                deferred.reject("Target is neither an element nor a number");
            } else if(duration < 0) {
                deferred.reject("Duration below 0, something is wrong");
            } else if(duration === 0) {
                element.scrollTop = targetValue;
                deferred.resolve();
            } else {
                var startTime = Date.now();
                var endTime = startTime + duration;

                var startTop = element.scrollTop;
                var distance = targetValue - startTop;

                var previousTop = element.scrollTop;

                var stop = $interval(function(){
                    if(element.scrollTop != previousTop) {
                        deferred.reject("Scroll interrupted");
                        $interval.cancel(stop);
                    }

                    var now = Date.now();
                    var point = MathUtilService.smootherStep(startTime, endTime, now);
                    var frameTop = Math.round(startTop + (distance * point));
                    element.scrollTop = frameTop;

                    if(now >= endTime) {
                        deferred.resolve();
                        $interval.cancel(stop);
                    }

                    if(element.scrollTop === previousTop && element.scrollTop !== frameTop) {
                        deferred.resolve();
                        $interval.cancel(stop);
                    }

                    previousTop = element.scrollTop;
                }, 0);

            }

            return deferred.promise;
        }

        function calculateElementTop(element) {
            var elementTop = 0;

            while(element) {
                elementTop += (element.offsetTop - element.scrollTop + element.clientTop);
                element = element.offsetParent;
            }

            return elementTop;
        }

    }
})();