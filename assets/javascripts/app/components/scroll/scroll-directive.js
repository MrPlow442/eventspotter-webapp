(function () {
    'use strict';

    angular
        .module('simpleScroll', []);

    angular
        .module('simpleScroll')
        .directive('scrollTo', scrollTo)
        .directive('scrollTarget', scrollTarget);

    scrollTo.$inject = ['$window', '$document', 'SimpleScrollService'];

    function scrollTo($window, $document, SimpleScrollService) {
        return {
            link: link,
            restrict: 'A',
            scope: {
                config: '=?'
            }
        };

        function link(scope, element, attrs) {

            if(angular.isUndefined(attrs.scrollTo)) {
                throw "Scroll to is missing the target name. Please set the target name. eg. scroll-to=\"MyTarget\"";
            }

            element.on("click", scrollToTarget);

            ////////////
            function scrollToTarget(event) {
                event.preventDefault();
                //console.log("SCROLL INVOKED");
                //console.log("TARGET IS", attrs.scrollTo);
                SimpleScrollService.scrollTo(attrs.scrollTo);
            }
        }
    }

    scrollTarget.$inject = ['SimpleScrollService'];

    function scrollTarget(SimpleScrollService) {
        return {
            link: link,
            restrict: 'A',
            scope: {}
        };

        function link(scope, element, attrs) {
            if(angular.isUndefined(attrs.scrollTarget)) {
                throw "Target missing name. Please set the target name. eg. scroll-target=\"MyTarget\"";
            }

            //console.log("REGISTERING ELEMENT", attrs.scrollTarget, "ELEMENT", element[0]);

            SimpleScrollService.registerTarget(attrs.scrollTarget, element[0]);
        }
    }

})();