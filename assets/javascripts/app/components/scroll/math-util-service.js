(function () {
    'use strict';

    angular
        .module('simpleScroll')
        .service('MathUtilService', MathUtilService);

    MathUtilService.$inject = [];

    function MathUtilService() {
        return {
            clamp: clamp,
            smoothStep: smoothStep,
            smootherStep: smootherStep
        };

        function clamp(value, min, max) {
            return Math.max(min, Math.min(max, value));
        }

        // https://en.wikipedia.org/wiki/Smoothstep
        function smoothStep(start, end, point) {
            point = clamp((point - start)/(end - start), 0, 1);
            return point * point * (3 - 2*point);
        }

        // https://en.wikipedia.org/wiki/Smoothstep
        function smootherStep(start, end, point) {
            point = clamp((point - start)/(end - start), 0, 1);
            return point * point * point * (point * (point * 6 - 15) + 10);
        }

    }
})();