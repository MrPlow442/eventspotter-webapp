(function () {
    'use strict';

    angular
        .module('app.components.service')
        .service('AlertService', AlertService);

    AlertService.$inject = ['$timeout'];

    function AlertService($timeout) {
        var _alerts = [];

        var DEFAULT_ERROR_TRANSLATIONS =  {
            400: 'ERROR_400',
            404: 'ERROR_404',
            500: 'ERROR_500',
            0: 'ERROR_SERVICE_UNAVAILABLE'
        };

        return {
            push: push,
            danger: danger,
            success: success,
            warning: warning,
            info: info,
            httpError: httpError,
            close: close,
            clear: clear,
            getAll: getAll
        };

        function push(params) {
            params.type = params.type || 'danger';
            params.timeout = params.timeout || 5000;

            var alert = {
                type: params.type,
                message: params.message,
                translate: params.translate,
                close: function() {
                    return close(this);
                }
            };

            if(params.timeout > 0 && angular.isNumber(params.timeout)) {
                $timeout(closeAlert, params.timeout);
            }

            function closeAlert() {
                alert.close();
            }
            return _alerts.push(alert);
        }

        function close(alert) {
            if(angular.isNumber(alert)) {
                return _alerts.splice(alert, 1);
            }
            if(angular.isObject(alert)) {
                var index = _alerts.indexOf(alert);
                if(index !== -1) {
                    return _alerts.splice(index, 1);
                }
            }
            return undefined;
        }

        function clear(){
            _alerts.splice(0, _alerts.length);
        }

        function getAll() {
            return _alerts;
        }

        function danger(options) {
            options = options || {};
            options.type = 'danger';

            push(options);
        }

        function success(options) {
            options = options || {};
            options.type = 'success';

            push(options);
        }

        function info(options) {
            options = options || {};
            options.type = 'info';

            push(options);
        }

        function warning(options) {
            options = options || {};
            options.type = 'warning';

            push(options);
        }

        function httpError(error, options) {
            if(angular.isUndefined(error)) {
                return;
            }

            options = options || {};

            var messages;
            var translations = angular.extend({}, DEFAULT_ERROR_TRANSLATIONS);
            if(angular.isDefined(options.messages)) {
                messages = options.messages;
            }
            if(angular.isDefined(options.translations)) {
                angular.extend(translations, options.translations);
            }

            var status = error.status;

            if(angular.isDefined(messages) && angular.isDefined(messages[status])) {
                danger({message: messages[status]});
            } else if (angular.isDefined(translations[status])) {
                danger({translate: translations[status]});
            } else {
                status = status >= 400 && status < 500 ? 400
                    : status >= 500 ? 500 : status;
                danger({translate: translations[status]});
            }

        }

    }
})();