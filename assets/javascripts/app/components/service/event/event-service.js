(function () {
    'use strict';

    angular
        .module('app.components.service')
        .service('EventService', EventService);

    EventService.$inject = ['$http', 'SERVICE_URL'];

    function EventService($http, SERVICE_URL) {
        return {
            events: events,
            event: event,
            create: create,
            update: update,
            rate: rate,
            addToFavorites: addToFavorites,
            removeFromFavorites: removeFromFavorites
        };

        function events(params) {
            var config = {
                url: SERVICE_URL + '/events',
                method: 'GET',
                params: params
            };

            return $http(config);
        }

        function event(id, params) {
            var config = {
              url: SERVICE_URL + '/events/' + id,
              method: 'GET',
              params: params
            };

            return $http(config);
        }

        function create(data) {
            var config = {
                url: SERVICE_URL + '/events',
                method: 'POST',
                data: data,
                auth: true
            };

            return $http(config);
        }

        function update(id, data) {
            var config = {
                url: SERVICE_URL + '/events/' + id,
                method: 'POST',
                data: data,
                auth: true
            };

            return $http(config);
        }

        function rate(id, data) {
            var config = {
                url: SERVICE_URL + '/events/' + id + '/rate',
                method: 'POST',
                data: data,
                auth: true
            };

            return $http(config);
        }

        function addToFavorites(id, data) {
            var config = {
                url: SERVICE_URL + '/events/' + id + '/favorites',
                method: 'POST',
                data: data,
                auth: true
            };

            return $http(config);
        }

        function removeFromFavorites(id, data) {
            var config = {
                url: SERVICE_URL + '/events/' + id + '/favorites',
                method: 'DELETE',
                data: data,
                auth: true
            };

            return $http(config);
        }
    }
})();