(function() {
    angular
        .module('app.components.service')
        .service('CodebookService', CodebookService);

    CodebookService.$inject = ['$http', 'SERVICE_URL'];

    function CodebookService($http, SERVICE_URL) {
        return {
            languages: languages,
            categories: categories,
            daysOfWeek: daysOfWeek
        };

        function languages(params) {
            var config = {
                url: SERVICE_URL + '/codebooks/languages',
                method: 'GET',
                params: params
            };

            return $http(config);
        }

        function categories(params) {
            var config = {
                url: SERVICE_URL + '/codebooks/categories',
                method: 'GET',
                params: params
            };

            return $http(config);
        }

        function daysOfWeek(params) {
            var config = {
                url: SERVICE_URL + '/codebooks/days-of-week',
                method: 'GET',
                params: params
            };

            return $http(config);
        }
    }
})();
