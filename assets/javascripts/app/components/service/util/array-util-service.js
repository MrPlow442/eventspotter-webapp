(function () {
    'use strict';

    angular
        .module('app.components.service')
        .service('ArrayUtilService', ArrayUtilService);

    ArrayUtilService.$inject = [];

    function ArrayUtilService() {
        return {
            angularIndexOf: angularIndexOf,
            indexOfByProperties: indexOfByProperties,
            pushUnique: pushUnique,
            removeIfExists: removeIfExists
        };

        function angularIndexOf(arr, obj){
            for(var i = 0; i < arr.length; i++) {
                if(angular.equals(arr[i], obj)) {
                    return i;
                }
            }
            return -1;
        }

        /**
         * Utility function which retrieves the index of an item in an array if provided properties match the provided item
         * @param array array to search
         * @param seekItem item to find
         * @param properties properties that have to match | can be array or string, for nested properties use dot notation inside a string or nested arrays
         * @returns {*}
         */
        function indexOfByProperties(array, seekItem, properties) {
            var props = [];

            if(angular.isArray(properties)) {
                props = properties;
            } else if(angular.isString(properties) && properties.indexOf(".") > -1) {
                props.push(properties.split("."));
            } else {
                props.push(properties);
            }

            var length = array.length;
            for(var i = 0; i < length; ++i) {
                var item = array[i];
                var propsLength = props.length;
                var matches = true;
                for(var j = 0; j < propsLength; ++j) {

                    if(angular.isString(props[j]) && item[props[j]] !== seekItem[props[j]]) {
                        matches = false;
                    }

                    if(angular.isArray(props[j])) {
                        var itemChain = item;
                        var seekChain = seekItem;
                        var chain = props[j];
                        var chainLength = chain.length;
                        for(var z = 0; z < chainLength; ++z) {
                            if(!itemChain || !seekChain) {
                                matches = false;
                            }
                            itemChain = itemChain[chain[z]];
                            seekChain = seekChain[chain[z]];
                        }

                        if(itemChain && seekChain && itemChain !== seekChain) {
                            matches = false;
                        }
                    }
                }
                if(matches) {
                    return i;
                }
            }
            return -1;
        }

        /**
         * Adds an item into the array only if the item isn't already in the array
         * @param arr
         * @param item
         */
        function pushUnique(arr, item) {
            if (angular.isUndefined(arr) || !angular.isArray(arr)) {
                return;
            }
            if (angularIndexOf(arr, item) === -1) {
                arr.push(item);
            }
        }

        /**
         * Removes the item from the array, if the item exists
         * @param arr
         * @param item
         */
        function removeIfExists(arr, item) {
            if (angular.isUndefined(arr) || !angular.isArray(arr)) {
                return;
            }
            var index = arr.indexOf(item);
            if (index !== -1) {
                arr.splice(index, 1);
            }
        }



    }
})();