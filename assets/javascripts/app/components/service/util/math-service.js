(function () {
    'use strict';

    angular
        .module('app.components.service')
        .service('MathService', MathService);

    MathService.$inject = [];

    function MathService() {
        var TEMPERATURE_CONVERSION_CONSTANT = 273.15;
        return {
            degreesToRadians: degreesToRadians,
            radiansToDegrees: radiansToDegrees,
            kelvinToCelsius: kelvinToCelsius,
            celsiusToKelvin: celsiusToKelvin,
            getDistanceFromLatLonInM: getDistanceFromLatLonInM,
            getDistanceBetween: getDistanceBetween,
            getMiddlePointFromLatLon: getMiddlePointFromLatLon,
            getMiddlePoint: getMiddlePoint
        };

        function degreesToRadians(degrees) {
            return degrees * (Math.PI/180);
        }

        function radiansToDegrees(radians) {
            return radians * (180/Math.PI);
        }

        function kelvinToCelsius(kelvin) {
            return kelvin - TEMPERATURE_CONVERSION_CONSTANT;
        }

        function celsiusToKelvin(celsius) {
            return celsius + TEMPERATURE_CONVERSION_CONSTANT;
        }

        function getDistanceFromLatLonInM(lat1,lon1,lat2,lon2) {
            var earthRadius = 6371000; //meters
            var rLat1 = degreesToRadians(lat1);
            var rLat2 = degreesToRadians(lat2);
            var dLon = degreesToRadians(lon2-lon1);

            return Math.acos(Math.sin(rLat1)*Math.sin(rLat2) + Math.cos(rLat1)*Math.cos(rLat2) * Math.cos(dLon)) * earthRadius;
        }

        function getDistanceBetween(point1, point2) {
            return getDistanceFromLatLonInM(point1.latitude, point1.longitude, point2.latitude, point2.longitude);
        }

        function getMiddlePointFromLatLon(lat1, lon1, lat2, lon2) {
            var dLon = degreesToRadians(lon2-lon1);
            var rLat1 = degreesToRadians(lat1);
            var rLat2 = degreesToRadians(lat2);
            var rLon1 = degreesToRadians(lon1);

            var Bx = Math.cos(rLat2) * Math.cos(dLon);
            var By = Math.cos(rLat2) * Math.sin(dLon);

            var lat3 = Math.atan2(Math.sin(rLat1) + Math.sin(rLat2), Math.sqrt((Math.cos(rLat1) + Bx) * (Math.cos(rLat1) + Bx) + By * By));
            var lon3 = rLon1 + Math.atan2(By, Math.cos(rLat1) + Bx);

            return {
                latitude: radiansToDegrees(lat3),
                longitude: radiansToDegrees(lon3)
            };
        }

        function getMiddlePoint(point1, point2) {
            return getMiddlePointFromLatLon(point1.latitude, point1.longitude, point2.latitude, point2.longitude);
        }
    }
})();