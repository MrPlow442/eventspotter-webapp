(function() {
    angular
        .module('app.components.service')
        .service('UserService', UserService);

    UserService.$inject = ['$http', 'SERVICE_URL'];

    function UserService($http, SERVICE_URL) {
        return {
            users: users,
            user: user,
            enable: enable,
            disable: disable,
            create: create,
            update: update,
            register: register,
            favorites: favorites,
            favorited: favorited,
            events: events
        };

        function users(params) {
            var config = {
                url: SERVICE_URL + '/users',
                method: 'GET',
                params: params,
                auth: true
            };

            return $http(config);
        }

        function user(identifier, params) {
            var config = {
                url: SERVICE_URL + '/users/' + identifier,
                method: 'GET',
                params: params,
                auth: true
            };

            return $http(config);
        }

        function enable(id, params) {
            var config = {
                url: SERVICE_URL + '/users/' + id + '/enable',
                method: 'PUT',
                params: params,
                auth: true
            };

            return $http(config);
        }

        function disable(id, params) {
            var config = {
                url: SERVICE_URL + '/users/' + id + '/disable',
                method: 'PUT',
                params: params,
                auth: true
            };

            return $http(config);
        }

        function create(data) {
            var config = {
                url: SERVICE_URL + '/users',
                method: 'POST',
                data: data,
                auth: true
            };

            return $http(config);
        }

        function update(id, data) {
            var config = {
                url: SERVICE_URL + '/users/' + id,
                method: 'PUT',
                data: data,
                auth: true
            };

            return $http(config);
        }

        function register(data) {
            var config = {
                url: SERVICE_URL + '/users/register',
                method: 'POST',
                data: data
            };

            return $http(config);
        }

        function favorites(identifier, params) {
            var config = {
                url: SERVICE_URL + '/users/' + identifier + '/favorites',
                method: 'GET',
                params: params,
                auth: true
            };

            return $http(config);
        }

        function favorited(identifier, eventId, params) {
            var config = {
                url: SERVICE_URL + '/users/' + identifier + '/favorited/' + eventId,
                method: 'GET',
                params: params,
                auth: true
            };

            return $http(config);
        }

        function events(identifier, params) {
            var config = {
                url: SERVICE_URL + '/users/' + identifier + '/events',
                method: 'GET',
                params: params,
                auth: true
            };

            return $http(config);
        }
    }
})();
